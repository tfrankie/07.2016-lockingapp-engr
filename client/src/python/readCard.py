#!/usr/bin/env python

import MFRC522

# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()

# Scan for cards
(status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

# If a card is found
#if status == MIFAREReader.MI_OK:
#    print "Card detected"

# Get the UID of the card
(status,uid) = MIFAREReader.MFRC522_Anticoll()

# If we have the UID, continue
if status == MIFAREReader.MI_OK:

    # Print UID
    print str(uid[0]) + str(uid[1])+str(uid[2])+str(uid[3])+str(uid[4])

else:
    print "NULL"