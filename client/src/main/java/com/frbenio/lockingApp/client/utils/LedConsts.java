package com.frbenio.lockingApp.client.utils;

import com.pi4j.io.gpio.*;

/**
 * Created by frben on 09.09.2016.
 */
public class LedConsts {

    public static final GpioPinDigitalOutput GREEN;
    public static final GpioPinDigitalOutput RED;

    static{
        GpioController gpio = GpioFactory.getInstance();
        GREEN = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_28, "GreenLED", PinState.LOW);
        RED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_29, "RedLED", PinState.LOW);

        GREEN.setShutdownOptions(true, PinState.LOW);
        RED.setShutdownOptions(true, PinState.LOW);

    }
}

