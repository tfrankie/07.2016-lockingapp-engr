package com.frbenio.lockingApp.client.gpio;

import com.pi4j.io.gpio.GpioPinDigitalOutput;

/**
 * Created by frben on 09.09.2016.
 */
public class LedManager {

    public static void turnOn(GpioPinDigitalOutput led){
        led.high();
    }

    public static void turnOff(GpioPinDigitalOutput led){
        led.low();
    }
}
