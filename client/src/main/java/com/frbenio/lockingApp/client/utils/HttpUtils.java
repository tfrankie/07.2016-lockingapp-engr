package com.frbenio.lockingApp.client.utils;

import com.frbenio.lockingApp.api.model.request.UnlockRequest;
import com.frbenio.lockingApp.api.model.response.UnlockResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

import javax.ws.rs.core.MediaType;

/**
 * Created by frben on 09.09.2016.
 */
public class HttpUtils {

    private static final String baseUrl = "http://192.168.137.1:8080/rest";
    private static Client httpClient;

    static {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        httpClient = Client.create(clientConfig);
        httpClient.setConnectTimeout(12);
    }

    public static UnlockResponse sendUnlockRequest(UnlockRequest request) {
        WebResource webResourcePost = httpClient.resource(baseUrl).path("/unlock");
        return webResourcePost.type(MediaType.APPLICATION_JSON).post(UnlockResponse.class, request);
    }

}
