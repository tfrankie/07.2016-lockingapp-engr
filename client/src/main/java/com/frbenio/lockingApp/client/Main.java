package com.frbenio.lockingApp.client;

import com.frbenio.lockingApp.client.gpio.LedManager;
import com.frbenio.lockingApp.client.rfid.CardReader;
import com.frbenio.lockingApp.client.utils.LedConsts;

/**
 * Created by frben on 09.09.2016.
 */
public class Main {

    public static void main(String[] args){
        CardReader cardReader = new CardReader();
        cardReader.startReading();
    }


    public static void exceptionCaught(Exception e){
        System.err.println("Exception: " + e.getMessage());
        for(int i=0; i<3; i++){
            LedManager.turnOn(LedConsts.RED);
            Main.sleep(300);
            LedManager.turnOff(LedConsts.RED);
            Main.sleep(300);
        }
    }

    public static void sleep(long milis){
        try {
            Thread.sleep(milis);
        } catch (InterruptedException e){
            Thread.currentThread().interrupt();
        }
    }
}
