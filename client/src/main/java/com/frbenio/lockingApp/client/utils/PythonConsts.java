package com.frbenio.lockingApp.client.utils;

/**
 * Created by frben on 09.09.2016.
 */
public class PythonConsts {

    private static final String SCRIPT_PATH = "/home/pi/workspace/lockingApp/client/src/python/readCard.py";
    public static final String[] SCRIPT_EXEC = { "python", SCRIPT_PATH };
    public static final String NULL = "NULL";

}
