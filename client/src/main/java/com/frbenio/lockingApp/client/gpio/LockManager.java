package com.frbenio.lockingApp.client.gpio;

import com.pi4j.io.gpio.*;

/**
 * Created by frben on 09.09.2016.
 */
public class LockManager {

    private static final GpioPinDigitalOutput lock;

    static{
        GpioController gpio = GpioFactory.getInstance();
        lock = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "Lock", PinState.LOW);
        lock.setShutdownOptions(true, PinState.LOW);
    }

    public static void unlock(){
        lock.high();
    }

    public static void lock(){
        lock.low();
    }
}
