package com.frbenio.lockingApp.client.rfid;

import com.frbenio.lockingApp.api.model.request.UnlockRequest;
import com.frbenio.lockingApp.api.model.response.UnlockResponse;
import com.frbenio.lockingApp.client.Main;
import com.frbenio.lockingApp.client.gpio.LedManager;
import com.frbenio.lockingApp.client.gpio.LockManager;
import com.frbenio.lockingApp.client.utils.HardwareUtils;
import com.frbenio.lockingApp.client.utils.HttpUtils;
import com.frbenio.lockingApp.client.utils.LedConsts;
import com.frbenio.lockingApp.client.utils.PythonConsts;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by frben on 09.09.2016.
 */
public class CardReader implements Cloneable {

    boolean reading = true;
    ProcessBuilder pythonScipt;

    public void startReading(){
        initCardReader();
        successfullyStarted();

        while(reading){
            try {
                String cardId = scanForCards();
                if(!cardId.equals(PythonConsts.NULL)){

                    UnlockRequest request = new UnlockRequest(HardwareUtils.LOCK_ID, cardId);
                    UnlockResponse response = HttpUtils.sendUnlockRequest(request);
                    handleResponse(response);

                }
            } catch (Exception e) {
                Main.exceptionCaught(e);
            }
        }
    }

    private void successfullyStarted(){
        for(int i=0; i<3; i++){
            LedManager.turnOn(LedConsts.GREEN);
            LedManager.turnOn(LedConsts.RED);
            Main.sleep(300);
            LedManager.turnOff(LedConsts.GREEN);
            LedManager.turnOff(LedConsts.RED);
            Main.sleep(300);
        }
    }
    private void initCardReader() {
        pythonScipt = new ProcessBuilder(PythonConsts.SCRIPT_EXEC);
        pythonScipt.redirectError();
    }

    private String scanForCards() throws IOException {
        Process p = pythonScipt.start();
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        return in.readLine();
    }


    private void handleResponse(UnlockResponse response) {
        if(response.getStatus().equals(Response.Status.OK) && response.isSuccessful()){
            LockManager.unlock();
            LedManager.turnOn(LedConsts.GREEN);
            Main.sleep(2000);
            LockManager.lock();
            LedManager.turnOff(LedConsts.GREEN);
        } else {
            LedManager.turnOn(LedConsts.RED);
            Main.sleep(2000);
            LedManager.turnOff(LedConsts.RED);
        }
    }
}
