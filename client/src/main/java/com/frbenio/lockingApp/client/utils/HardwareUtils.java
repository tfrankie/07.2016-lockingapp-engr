package com.frbenio.lockingApp.client.utils;

import java.net.NetworkInterface;
import java.net.SocketException;

/**
 * Created by frben on 09.09.2016.
 */
public class HardwareUtils {

    public static final String LOCK_ID;

    static{
        LOCK_ID = getLockId();
    }

    private static String getLockId(){
        try {
            NetworkInterface network = NetworkInterface.getByName("eth0");
            byte[] mac = network.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            return sb.toString();
        } catch (SocketException e){
            e.printStackTrace();
        }
        return null;
    }

}
