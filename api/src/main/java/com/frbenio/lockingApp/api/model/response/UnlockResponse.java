package com.frbenio.lockingApp.api.model.response;


import javax.ws.rs.core.Response;

/**
 * Created by frben on 24.08.2016.
 */
public class UnlockResponse {

    Response.Status status;
    boolean successful;

    public Response.Status getStatus() {
        return status;
    }

    public void setStatus(Response.Status status) {
        this.status = status;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
}
