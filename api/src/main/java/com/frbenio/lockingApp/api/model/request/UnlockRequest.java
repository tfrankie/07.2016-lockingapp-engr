package com.frbenio.lockingApp.api.model.request;

/**
 * Created by frben on 24.08.2016.
 */
public class UnlockRequest {

    String lockId;
    String cardId;

    public UnlockRequest(){

    }

    public UnlockRequest(String lockId, String cardId) {
        this.lockId = lockId;
        this.cardId = cardId;
    }

    public String getLockId() {
        return lockId;
    }

    public void setLockId(String lockId) {
        this.lockId = lockId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}
