package com.frbenio.lockingApp.config;

import com.frbenio.lockingApp.jsf.utils.ViewScopeImpl;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by frben on 26.08.2016.
 */
@Configuration
public class ViewScopeFactory {

    @Bean
    public ViewScopeImpl viewScope(){
        return new ViewScopeImpl();
    }

    @Bean
    public CustomScopeConfigurer scopeConfigurer(ViewScopeImpl viewScope){
        CustomScopeConfigurer scopeConfigurer = new CustomScopeConfigurer();
        Map<String, Object> scopes = new HashMap<>();
        scopes.put("view", viewScope);
        scopeConfigurer.setScopes(scopes);
        return scopeConfigurer;
    }

}