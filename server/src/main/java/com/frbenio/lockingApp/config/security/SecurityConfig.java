package com.frbenio.lockingApp.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private SecurityAuthProvider authProvider;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/rest/**").permitAll()
			.antMatchers("/javax.faces.resource/**").permitAll()

			.antMatchers("/**").authenticated()
			.and().formLogin().loginPage("/views/login.xhtml")
				.usernameParameter("login")
				.passwordParameter("password")
			.permitAll()
			.failureUrl("/views/login.xhtml?error=1")

			.and().csrf().disable();
	}


}  
