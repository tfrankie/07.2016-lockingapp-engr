package com.frbenio.lockingApp.config;

import com.frbenio.lockingApp.rest.RestController;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by frben on 30.07.2016.
 */
public class RestConfig extends ResourceConfig {

    public RestConfig() {
        super(
                RestController.class,
                JacksonFeature.class
        );
    }
}
