package com.frbenio.lockingApp.config.security;

import com.frbenio.lockingApp.database.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by frben on 24.09.2016.
 */
@Component
public class SecurityAuthProvider implements AuthenticationProvider {

    private AdminService adminService;

    @Autowired
    public SecurityAuthProvider(AdminService adminService) {
        this.adminService = adminService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String login = authentication.getName();
        String password = authentication.getCredentials().toString();

        if (adminService.checkLoginAutorization(login, password)) {
            return new UsernamePasswordAuthenticationToken(login, password, new ArrayList<>());
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
