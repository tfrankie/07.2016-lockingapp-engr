package com.frbenio.lockingApp.database.service;

import com.frbenio.lockingApp.database.model.Group;
import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.database.model.Room;
import com.frbenio.lockingApp.database.model.Space;
import com.frbenio.lockingApp.database.repository.SpaceRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by frben on 26.11.2016.
 */
@Service
public class SpaceService {

    private final SpaceRepository repository;

    @Autowired
    public SpaceService(SpaceRepository repository) {
        this.repository = repository;
    }

    public void save(Space newSpace) {
        repository.save(newSpace);
    }

    public List<Group> getFirstTierGroups() {
        return repository.getFirstTierGroups();
    }

    public List<Room> getFirstTierRooms() {
        return repository.getFirstTierRooms();
    }

    public void deleteSpace(Space space) {
        repository.delete(space);
    }

    public Room findRoomByLockId(String lockId) {
        return repository.findRoomByLockId(lockId);
    }

    public Group getGroupById(ObjectId mongoId) {
        return repository.getGroupById(mongoId);
    }

    public Room findByLockId(String lockId) {
        return repository.getRoomByLockId(lockId);
    }

    public boolean checkUnlockPermission(Room room, Role role) {
        return role != null && role.getRooms() != null && role.getRooms().contains(room);
    }
}
