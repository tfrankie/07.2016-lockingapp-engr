package com.frbenio.lockingApp.database.model;

import com.frbenio.lockingApp.database.model.consts.MongoCollections;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by frben on 25.11.2016.
 */
@Document(collection = MongoCollections.ROOMS)
public class Room extends Space {

    private String lockId;

    public Room(){

    }

    public Room(Room editingRoom) {
        this.mongoId = editingRoom.getMongoId();
        this.lockId = editingRoom.getLockId();
        this.name = editingRoom.getName();
        this.root = editingRoom.isRoot();
    }

    public String getLockId() {
        return lockId;
    }

    public void setLockId(String lockId) {
        this.lockId = lockId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        if (mongoId != null ? !mongoId.equals(room.mongoId) : room.mongoId != null) return false;
        if (lockId != null ? !lockId.equals(room.lockId) : room.lockId != null) return false;
        return name != null ? name.equals(room.name) : room.name == null;

    }

    @Override
    public int hashCode() {
        int result = mongoId != null ? mongoId.hashCode() : 0;
        result = 31 * result + (lockId != null ? lockId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Room{" +
                "name='" + name + '\'' +
                ", lockId='" + lockId + '\'' +
                ", root='" + root + '\'' +
                '}';
    }
}
