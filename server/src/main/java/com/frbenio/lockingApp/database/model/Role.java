package com.frbenio.lockingApp.database.model;

import com.frbenio.lockingApp.database.model.consts.MongoCollections;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

/**
 * Created by frben on 24.10.2016.
 */
@Document(collection = MongoCollections.ROLES)
public class Role implements Serializable {

    @Id
    private ObjectId mongoId;
    private String name;
    @DBRef(lazy = true)
    private List<Room> rooms;

    public ObjectId getMongoId() {
        return mongoId;
    }
    public void setMongoId(ObjectId mongoId) {
        this.mongoId = mongoId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<Room> getRooms() {
        return rooms;
    }
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (mongoId != null ? !mongoId.equals(role.mongoId) : role.mongoId != null) return false;
        return name != null ? name.equals(role.name) : role.name == null;

    }

    @Override
    public int hashCode() {
        int result = mongoId != null ? mongoId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Role{" +
                "mongoId=" + mongoId +
                ", name='" + name + '\'' +
                ", rooms=" + rooms +
                '}';
    }
}
