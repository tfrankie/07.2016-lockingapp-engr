package com.frbenio.lockingApp.database.repository;

import com.frbenio.lockingApp.database.model.Group;
import com.frbenio.lockingApp.database.model.Room;
import com.frbenio.lockingApp.database.model.Space;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by frben on 26.11.2016.
 */
@Component
public class SpaceRepository {

    private MongoOperations mongoTemplate;

    @Autowired
    public SpaceRepository(MongoOperations mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public void save(Space space) {
        mongoTemplate.save(space);
    }

    public List<Group> getFirstTierGroups() {
        Query query = new Query();
        query.addCriteria(Criteria
                .where("root").is(true));
        return mongoTemplate.find(query, Group.class);
    }

    public List<Room> getFirstTierRooms() {
        Query query = new Query();
        query.addCriteria(Criteria
                .where("root").is(true));
        return mongoTemplate.find(query, Room.class);
    }

    public void delete(Space space) {
        mongoTemplate.remove(space);
    }

    public Room findRoomByLockId(String lockId) {
        Query query = new Query();
        query.addCriteria(Criteria
                .where("lockId").is(lockId));
        return mongoTemplate.findOne(query, Room.class);
    }

    public Group getGroupById(ObjectId mongoId) {
        Query query = new Query();
        query.addCriteria(Criteria
                .where("_id").is(mongoId));
        return mongoTemplate.findOne(query, Group.class);
    }

    public Room getRoomByLockId(String lockId) {
        Query query = new Query();
        query.addCriteria(Criteria
                .where("lockId").is(lockId));
        return mongoTemplate.findOne(query, Room.class);
    }
}
