package com.frbenio.lockingApp.database.service;

import com.frbenio.lockingApp.database.model.Admin;
import com.frbenio.lockingApp.database.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by frben on 25.09.2016.
 */
@Service
public class AdminService {

    private AdminRepository repository;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AdminService(AdminRepository repository) {
        this.repository = repository;
        passwordEncoder = new BCryptPasswordEncoder();

        createAdminIfNotExists();
    }

    private void createAdminIfNotExists() {
        List<Admin> admins = repository.findAll();
        if(admins.size() == 0){
            Admin admin = new Admin();
            admin.setLogin("admin");
            admin.setPassword(passwordEncoder.encode("admin"));
            repository.save(admin);
        }
    }

    public boolean checkLoginAutorization(String login, String password) {
        Admin admin = repository.getByLogin(login);
        return admin != null && passwordEncoder.matches(password, admin.getPassword());
    }

    public void updateLogin(String newLogin) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();

        Admin admin = repository.getByLogin(login);
        admin.setLogin(newLogin);
        repository.save(admin);
    }

    public void updatePassword(String newPassword) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();

        Admin admin = repository.getByLogin(login);
        admin.setPassword(newPassword);
        repository.save(admin);
    }

    public boolean validateCurrentPassword(String password){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();

        Admin admin = repository.getByLogin(login);
        return admin != null && admin.getPassword().equals(password);
    }

}
