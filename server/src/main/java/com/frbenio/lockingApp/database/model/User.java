package com.frbenio.lockingApp.database.model;

import com.frbenio.lockingApp.database.model.consts.MongoCollections;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Created by frben on 24.08.2016.
 */
@Document(collection = MongoCollections.USERS)
public class User implements Serializable {

    @Id
    private ObjectId mongoId;
    private String cardId;
    private String name;
    @DBRef
    private Role role;

    public User(){

    }

    public User(User user) {
        this.mongoId = user.getMongoId();
        this.cardId = user.getCardId();
        this.name = user.getName();
        this.role = user.getRole();
    }

    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }
    public String getCardId() {
        return cardId;
    }
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
    public ObjectId getMongoId() {
        return mongoId;
    }
    public void setMongoId(ObjectId mongoId) {
        this.mongoId = mongoId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (cardId != null ? !cardId.equals(user.cardId) : user.cardId != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        return role != null ? role.equals(user.role) : user.role == null;

    }

    @Override
    public int hashCode() {
        int result = cardId != null ? cardId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "mongoId=" + mongoId +
                ", cardId='" + cardId + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
