package com.frbenio.lockingApp.database.repository;

import com.frbenio.lockingApp.database.model.Admin;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by frben on 25.09.2016.
 */
public interface AdminRepository extends MongoRepository<Admin, String> {

    List<Admin> findAll();
    Admin getByLogin(String login);
    Admin save(Admin door);

}
