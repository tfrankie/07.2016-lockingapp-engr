package com.frbenio.lockingApp.database.model.consts;

/**
 * Created by frben on 24.08.2016.
 */
public class MongoCollections {

    public static final String USERS = "users";
    public static final String ADMINS = "admins";
    public static final String ROLES = "roles";

    public static final String GROUPS = "groups";
    public static final String ROOMS = "rooms";
}
