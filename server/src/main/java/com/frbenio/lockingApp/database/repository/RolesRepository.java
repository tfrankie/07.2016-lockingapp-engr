package com.frbenio.lockingApp.database.repository;

import com.frbenio.lockingApp.database.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by frben on 24.10.2016.
 */
public interface RolesRepository extends MongoRepository<Role, String> {

    List<Role> findAll();
    Role save(Role door);
    void delete(Role door);

}