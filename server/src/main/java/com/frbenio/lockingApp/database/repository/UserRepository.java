package com.frbenio.lockingApp.database.repository;

import com.frbenio.lockingApp.database.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by frben on 24.08.2016.
 */
public interface UserRepository extends MongoRepository<User, String> {

    List<User> findAll();
    User findByCardId(String id);
    void delete(User user);
    User save(User user);

}
