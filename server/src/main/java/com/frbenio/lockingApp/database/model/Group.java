package com.frbenio.lockingApp.database.model;

import com.frbenio.lockingApp.database.model.consts.MongoCollections;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by frben on 25.11.2016.
 */
@Document(collection = MongoCollections.GROUPS)
public class Group extends Space {

    @DBRef(lazy = true)
    private List<Group> groups;
    @DBRef(lazy = true)
    private List<Room> rooms;

    public Group(){

    }

    public Group(Group editingGroup) {
        this.mongoId = editingGroup.getMongoId();
        this.name = editingGroup.getName();
        this.root = editingGroup.isRoot();
        this.groups = editingGroup.getGroups();
        this.rooms = editingGroup.getRooms();
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (mongoId != null ? !mongoId.equals(group.mongoId) : group.mongoId != null) return false;
        return name != null ? name.equals(group.name) : group.name == null;

    }

    @Override
    public int hashCode() {
        int result = mongoId != null ? mongoId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name=" + name +
                ", groups=" + groups + '\'' +
                ", rooms=" + rooms + '\'' +
                ", root='" + root + '\'' +
                '}';
    }
}
