package com.frbenio.lockingApp.database.service;

import com.frbenio.lockingApp.database.model.User;
import com.frbenio.lockingApp.database.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by frben on 24.08.2016.
 */
@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> fetchAll(){
        return userRepository.findAll();
    }

    public User findByCardId(String id){
        return userRepository.findByCardId(id);
    }

    public void delete(User user) {
        userRepository.delete(user);
    }

    public void save(User user) {
        userRepository.save(user);
    }
}
