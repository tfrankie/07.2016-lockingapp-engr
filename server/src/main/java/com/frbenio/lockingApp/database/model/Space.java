package com.frbenio.lockingApp.database.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 * Created by frben on 25.11.2016.
 */
public abstract class Space {

    @Id
    protected ObjectId mongoId;
    protected String name;
    protected boolean root;

    public ObjectId getMongoId() {
        return mongoId;
    }

    public void setMongoId(ObjectId mongoId) {
        this.mongoId = mongoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRoot() {
        return root;
    }

    public void setRoot(boolean root) {
        this.root = root;
    }

    @Override
    public String toString() {
        return "Space{" +
                "mongoId=" + mongoId +
                ", name='" + name + '\'' +
                ", root='" + root + '\'' +
                '}';
    }
}
