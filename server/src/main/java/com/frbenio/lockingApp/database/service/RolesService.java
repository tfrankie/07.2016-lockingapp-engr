package com.frbenio.lockingApp.database.service;

import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.database.repository.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by frben on 24.10.2016.
 */
@Service
public class RolesService {

    private RolesRepository rolesRepository;

    @Autowired
    public RolesService(RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    public List<Role> getRoles() {
        return rolesRepository.findAll();
    }

    public void addRole(Role newRole) {
        rolesRepository.save(newRole);
    }

    public void deleteRole(Role role) {
        rolesRepository.delete(role);
    }

    public void save(Role role) {
        rolesRepository.save(role);
    }
}
