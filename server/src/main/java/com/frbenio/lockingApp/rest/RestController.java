package com.frbenio.lockingApp.rest;

import com.frbenio.lockingApp.api.model.request.UnlockRequest;
import com.frbenio.lockingApp.api.model.response.UnlockResponse;
import com.frbenio.lockingApp.database.model.Room;
import com.frbenio.lockingApp.database.model.User;
import com.frbenio.lockingApp.database.service.SpaceService;
import com.frbenio.lockingApp.database.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by frben on 30.07.2016.
 */
@Path("/unlock")
public class RestController {

    final static Logger logger = Logger.getLogger(RestController.class);

    private final SpaceService spaceService;
    private final UserService userService;

    @Autowired
    public RestController(SpaceService spaceService, UserService userService) {
        this.spaceService = spaceService;
        this.userService = userService;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UnlockResponse unlock(UnlockRequest request) {
        UnlockResponse response = new UnlockResponse();

        Room room = spaceService.findByLockId(request.getLockId());
        if(room == null){
            response.setStatus(Response.Status.BAD_REQUEST);
            return response;
        }

        User user = userService.findByCardId(request.getCardId());
        if(user == null){
            response.setStatus(Response.Status.UNAUTHORIZED);
            return response;
        }

        response.setStatus(Response.Status.OK);
        boolean successful = spaceService.checkUnlockPermission(room, user.getRole());
        response.setSuccessful(successful);

        logger.info("lockId: " + request.getLockId() + " cardId: " + request.getCardId() + " successful: " + successful);

        return response;
    }

}
