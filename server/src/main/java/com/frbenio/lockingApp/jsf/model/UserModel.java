package com.frbenio.lockingApp.jsf.model;

import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.database.model.User;
import com.frbenio.lockingApp.jsf.controller.UserController;
import com.frbenio.lockingApp.jsf.utils.consts.Messages;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by frben on 26.08.2016.
 */
@Component
@Scope("view")
public class UserModel implements Serializable {

    private final UserController controller;

    private List<User> users;
    private User newUser = new User();

    private User copyOfEditingUser;
    private User editingUser;

    private List<Role> allRoles;

    @Autowired
    public UserModel(UserController controller) {
        this.controller = controller;
    }

    @PostConstruct
    public void init(){
        users = controller.getUsers();
        allRoles = controller.getAllRoles();
    }

    public void addUser(){
        if(controller.idAlreadyExists(newUser.getCardId())){
            FacesContext.getCurrentInstance().addMessage(Messages.NEW_USER_MESSAGE_ID, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.ID_ALREADY_EXISTS_LABEL, null));
        } else {
            RequestContext.getCurrentInstance().execute("PF('addDialog').hide();");
            users.add(newUser);
            controller.addUser(newUser);
            FacesContext.getCurrentInstance().addMessage(Messages.USER_MESSAGE_ID, new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.USER_ADDED_LABEL, Messages.USER_ADDED_MESSAGE));
        }
        newUser = new User();
    }

    public void editUser(User user) {
        editingUser = user;
        copyOfEditingUser = new User(editingUser);
        RequestContext.getCurrentInstance().execute("PF('editDialog').show();");
    }

    public void saveUser(){
        if(controller.idAlreadyExists(copyOfEditingUser)){
            FacesContext.getCurrentInstance().addMessage(Messages.EDIT_USER_MESSAGE_ID, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.ID_ALREADY_EXISTS_LABEL, null));
        } else {
            RequestContext.getCurrentInstance().execute("PF('editDialog').hide();");
            controller.saveUser(copyOfEditingUser);
            applyEditedUser();
        }
    }

    private void applyEditedUser() {
        editingUser.setName(copyOfEditingUser.getName());
        editingUser.setCardId(copyOfEditingUser.getCardId());
        editingUser.setRole(copyOfEditingUser.getRole());
    }

    public void deleteUser(User user){
        users.remove(user);

        controller.deleteUser(user);
    }

    public List<Role> getAllRoles() {
        return allRoles;
    }
    public void setAllRoles(List<Role> allRoles) {
        this.allRoles = allRoles;
    }
    public User getCopyOfEditingUser() {
        return copyOfEditingUser;
    }
    public void setCopyOfEditingUser(User copyOfEditingUser) {
        this.copyOfEditingUser = copyOfEditingUser;
    }
    public List<User> getUsers() {
        return users;
    }
    public void setUsers(List<User> users) {
        this.users = users;
    }
    public User getNewUser() {
        return newUser;
    }
    public void setNewUser(User newUser) {
        this.newUser = newUser;
    }
}
