package com.frbenio.lockingApp.jsf.model;

import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.jsf.controller.RolesController;
import com.frbenio.lockingApp.jsf.utils.consts.Messages;
import com.frbenio.lockingApp.jsf.utils.consts.ParamId;
import com.frbenio.lockingApp.jsf.utils.consts.ViewId;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by frben on 24.10.2016.
 */
@Component
@Scope("view")
public class RolesModel implements Serializable {

    private final RolesController controller;

    private List<Role> roles;
    private Role newRole = new Role();

    @Autowired
    public RolesModel(RolesController controller) {
        this.controller = controller;

        roles = controller.getRoles();
    }

    public void addRole(){
        RequestContext.getCurrentInstance().execute("PF('addDialog').hide();");
        roles.add(newRole);
        controller.addRole(newRole);
        FacesContext.getCurrentInstance().addMessage(Messages.ROLES_MESSAGE_ID, new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                            Messages.ROLE_ADDED_LABEL, Messages.ROLE_ADDED_MESSAGE));

        newRole = new Role();
    }

    public void deleteRole(Role role){
        roles.remove(role);

        controller.deleteRole(role);
    }

    public void onRowSelect(SelectEvent event) throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.getExternalContext().getFlash().put(ParamId.ROLE, event.getObject());
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, ViewId.EDIT_ROLE);
    }

    public List<Role> getRoles() {
        return roles;
    }
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    public Role getNewRole() {
        return newRole;
    }
    public void setNewRole(Role newRole) {
        this.newRole = newRole;
    }
}
