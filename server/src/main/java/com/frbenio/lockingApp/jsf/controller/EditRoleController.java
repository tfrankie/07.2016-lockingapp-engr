package com.frbenio.lockingApp.jsf.controller;

import com.frbenio.lockingApp.database.model.Group;
import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.database.model.Room;
import com.frbenio.lockingApp.database.service.RolesService;
import com.frbenio.lockingApp.database.service.SpaceService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by frben on 27.08.2016.
 */
@Component
@Scope("singleton")
public class EditRoleController {

    private final RolesService rolesService;
    private final SpaceService spaceService;

    @Autowired
    public EditRoleController(RolesService rolesService, SpaceService spaceService) {
        this.rolesService = rolesService;
        this.spaceService = spaceService;
    }

    public void save(Role role) {
        rolesService.save(role);
    }

    public List<Group> getFirstTierGroups() {
        return spaceService.getFirstTierGroups();
    }

    public List<Room> getFirstTierRooms() {
        return spaceService.getFirstTierRooms();
    }

    public List<Room> getOnlyRoomChildren(Group group) {
        List<Room> children = new ArrayList<>();
        if(group.getGroups() != null){
            for (Group childGroup : group.getGroups()) {
                children.addAll(getOnlyRoomChildren(childGroup));
            }
        }
        if(group.getRooms() != null){
            children.addAll(group.getRooms());
        }
        return children;
    }

    public Group getGroupById(ObjectId mongoId) {
        return spaceService.getGroupById(mongoId);
    }
}
