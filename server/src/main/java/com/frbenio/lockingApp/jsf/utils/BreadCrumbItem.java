package com.frbenio.lockingApp.jsf.utils;

import org.bson.types.ObjectId;

/**
 * Created by frben on 26.11.2016.
 */
public class BreadCrumbItem {

    private ObjectId mongoId;
    private String name;

    public BreadCrumbItem(ObjectId mongoId, String name) {
        this.mongoId = mongoId;
        this.name = name;
    }

    public ObjectId getMongoId() {
        return mongoId;
    }

    public void setMongoId(ObjectId mongoId) {
        this.mongoId = mongoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BreadCrumbItem{" +
                "mongoId=" + mongoId +
                ", name='" + name + '\'' +
                '}';
    }
}
