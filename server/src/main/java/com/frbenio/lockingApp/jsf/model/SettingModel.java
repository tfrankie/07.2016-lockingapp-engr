package com.frbenio.lockingApp.jsf.model;

import com.frbenio.lockingApp.jsf.controller.SettingController;
import com.frbenio.lockingApp.jsf.utils.consts.Messages;
import com.frbenio.lockingApp.jsf.utils.consts.ViewId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.IOException;

/**
 * Created by frben on 25.09.2016.
 */
@Component
@Scope("view")
public class SettingModel {

    private String newLogin;
    private String password;
    private String newPassword;
    private String newPasswordRepeated;

    private SettingController controller;

    @Autowired
    public SettingModel(SettingController controller) {
        this.controller = controller;
    }

    public String changeLogin() throws IOException {
        if(controller.validateCurrentPassword(password)){
            controller.changeLogin(newLogin);
            FacesContext.getCurrentInstance().addMessage(Messages.SETTINGS_MESSAGE, new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.LOGIN_CHANGED_TITLE, Messages.LOGIN_CHANGED_DESCRIPTION));
            clearInputs();
        } else {
            FacesContext.getCurrentInstance().addMessage(Messages.CHANGE_LOGIN_PASSWORD, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.WRONG_PASSWORD, null));
        }
        return null;
    }

    public String changePassword(){
        boolean validated = true;

        if(!controller.validateCurrentPassword(password)){
            validated = false;
            FacesContext.getCurrentInstance().addMessage(Messages.CHANGE_PASSWORD_OLD_PASSWORD, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.WRONG_PASSWORD, null));
        }
        if(!newPasswordRepeated.equals(newPassword)){
            validated = false;
            FacesContext.getCurrentInstance().addMessage(Messages.REPEAT_PASSWORD_ID, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.WRONG_REPEATED_PASSWORD, null));
        }
        if(validated){
            controller.changePassword(newPassword);
            FacesContext.getCurrentInstance().addMessage(Messages.SETTINGS_MESSAGE, new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.PASSWORD_CHANGED_TITLE, Messages.PASSWORD_CHANGED_DESCRIPTION));
            return ViewId.SETTINGS;
        }
        return null;
    }

    private void clearInputs() {
        newLogin = null;
        password = null;
        newPassword = null;
        newPasswordRepeated = null;
    }

    public String getNewLogin() {
        return newLogin;
    }

    public void setNewLogin(String newLogin) {
        this.newLogin = newLogin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordRepeated() {
        return newPasswordRepeated;
    }

    public void setNewPasswordRepeated(String newPasswordRepeated) {
        this.newPasswordRepeated = newPasswordRepeated;
    }
}
