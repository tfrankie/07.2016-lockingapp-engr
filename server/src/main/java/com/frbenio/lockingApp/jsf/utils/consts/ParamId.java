package com.frbenio.lockingApp.jsf.utils.consts;

/**
 * Created by frben on 27.08.2016.
 */
public class ParamId {

    public static final String GROUP = "passingGroup";
    public static final String ROLE = "passingRole";
    public static final String BREADCRUMB_MODEL = "breadCrumbModel";

}
