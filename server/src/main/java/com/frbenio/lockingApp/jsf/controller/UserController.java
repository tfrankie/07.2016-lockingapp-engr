package com.frbenio.lockingApp.jsf.controller;

import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.database.model.User;
import com.frbenio.lockingApp.database.service.RolesService;
import com.frbenio.lockingApp.database.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by frben on 26.08.2016.
 */
@Component
@Scope("singleton")
public class UserController {

    private final UserService userService;
    private final RolesService rolesService;

    @Autowired
    public UserController(UserService userService, RolesService rolesService) {
        this.userService = userService;
        this.rolesService = rolesService;
    }

    public List<User> getUsers() {
        return userService.fetchAll();
    }

    public List<Role> getAllRoles() {
        return rolesService.getRoles();
    }

    public void addUser(User user) {
        userService.save(user);
    }

    public void deleteUser(User user){
        userService.delete(user);
    }

    public boolean idAlreadyExists(String cardId) {
        return userService.findByCardId(cardId) != null;
    }

    public boolean idAlreadyExists(User editingUser) {
        User userById = userService.findByCardId(editingUser.getCardId());
        return userById != null && !userById.getMongoId().equals(editingUser.getMongoId());
    }

    public void saveUser(User user) {
        userService.save(user);
    }

}
