package com.frbenio.lockingApp.jsf.utils.consts;

/**
 * Created by frben on 27.08.2016.
 */
public class ViewId {

    public static final String EDIT_ROLE = "edit-role";
    public static final String SETTINGS = "settings";

    public static final String SPACES = "spaces";

}
