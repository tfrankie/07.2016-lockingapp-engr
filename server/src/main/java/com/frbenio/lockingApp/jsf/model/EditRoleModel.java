package com.frbenio.lockingApp.jsf.model;

import com.frbenio.lockingApp.database.model.Group;
import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.database.model.Room;
import com.frbenio.lockingApp.jsf.controller.EditRoleController;
import com.frbenio.lockingApp.jsf.utils.BreadCrumbItem;
import com.frbenio.lockingApp.jsf.utils.consts.ParamId;
import com.frbenio.lockingApp.jsf.utils.consts.ViewId;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by frben on 28.08.2016.
 */
@Component
@Scope("view")
public class EditRoleModel implements Serializable {

    private final EditRoleController controller;

    private Role role;

    private Group currentTierGroup;
    private List<Group> currentTierGroups;
    private List<Room> currentTierRooms;
    private List<BreadCrumbItem> breadCrumbModel;

    @Autowired
    public EditRoleModel(EditRoleController controller) {
        this.controller = controller;
    }

    @PostConstruct
    public void init(){
        role = (Role) FacesContext.getCurrentInstance().getExternalContext().getFlash().get(ParamId.ROLE);

        currentTierGroup = (Group) FacesContext.getCurrentInstance().getExternalContext().getFlash().get(ParamId.GROUP);
        if (currentTierGroup == null) {
            currentTierGroups = controller.getFirstTierGroups();
            currentTierRooms = controller.getFirstTierRooms();
        } else {
            currentTierGroups = currentTierGroup.getGroups();
            currentTierRooms = currentTierGroup.getRooms();

            breadCrumbModel = (List<BreadCrumbItem>) FacesContext.getCurrentInstance().getExternalContext().getFlash().get(ParamId.BREADCRUMB_MODEL);
        }
    }

    public void addGroup(Group group){
        List<Room> children = controller.getOnlyRoomChildren(group);
        if(role.getRooms() == null){
            role.setRooms(new ArrayList<>());
        }
        role.getRooms().addAll(children);
        controller.save(role);
    }

    public void removeGroup(Group group){
        List<Room> children = controller.getOnlyRoomChildren(group);
        role.getRooms().removeAll(children);
        controller.save(role);
    }

    public void addRoom(Room room){
        if(role.getRooms() == null){
            role.setRooms(new ArrayList<>());
        }
        role.getRooms().add(room);
        controller.save(role);
    }

    public void removeRoom(Room room){
        role.getRooms().remove(room);
        controller.save(role);
    }

    public void saveRole(){
        RequestContext.getCurrentInstance().execute("PF('editDialog').hide();");
        controller.save(role);
    }

    public void onRowSelect(SelectEvent event) throws IOException {
        Group group = (Group) event.getObject();

        if(breadCrumbModel == null){
            breadCrumbModel = new ArrayList<>();
        }
        breadCrumbModel.add(new BreadCrumbItem(group.getMongoId(), group.getName()));

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Flash flash = facesContext.getExternalContext().getFlash();
        flash.put(ParamId.ROLE, role);
        flash.put(ParamId.GROUP, group);
        flash.put(ParamId.BREADCRUMB_MODEL, breadCrumbModel);
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, ViewId.EDIT_ROLE);
    }

    public String openRootGroup(){
        Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
        flash.put(ParamId.ROLE, role);
        return ViewId.EDIT_ROLE;
    }

    public String openGroup(BreadCrumbItem groupItem) {
        int groupIndex = breadCrumbModel.indexOf(groupItem);
        for(int i=breadCrumbModel.size()-1; i>=0; i--){
            if(i > groupIndex){
                breadCrumbModel.remove(i);
            }
        }

        Group group = controller.getGroupById(groupItem.getMongoId());

        Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
        flash.put(ParamId.ROLE, role);
        flash.put(ParamId.GROUP, group);
        flash.put(ParamId.BREADCRUMB_MODEL, breadCrumbModel);
        return ViewId.EDIT_ROLE;
    }

    public boolean disableFold(BreadCrumbItem item){
        return item.getMongoId().equals(currentTierGroup.getMongoId());
    }

    public boolean groupIsAdded(Group group){
        List<Room> children = controller.getOnlyRoomChildren(group);
        return role.getRooms() != null && !children.isEmpty() &&  role.getRooms().containsAll(children);
    }

    public boolean doorIsAdded(Room room) {
        return role.getRooms() != null && role.getRooms().contains(room);
    }

    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }
    public List<Group> getCurrentTierGroups() {
        return currentTierGroups;
    }
    public void setCurrentTierGroups(List<Group> currentTierGroups) {
        this.currentTierGroups = currentTierGroups;
    }
    public List<Room> getCurrentTierRooms() {
        return currentTierRooms;
    }
    public void setCurrentTierRooms(List<Room> currentTierRooms) {
        this.currentTierRooms = currentTierRooms;
    }
    public List<BreadCrumbItem> getBreadCrumbModel() {
        return breadCrumbModel;
    }
}