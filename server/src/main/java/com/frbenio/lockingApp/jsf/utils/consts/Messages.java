package com.frbenio.lockingApp.jsf.utils.consts;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by frben on 01.09.2016.
 */
public class Messages {

    private static final ResourceBundle resourceBundle = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "i18n");

    public static final String DOOR_MESSAGE_ID = "doorMessages";
    public static final String USER_MESSAGE_ID = "userMessages";
    public static final String NEW_DOOR_MESSAGE_ID = "newDoorLockId";
    public static final String NEW_USER_MESSAGE_ID = "newUserCardId";
    public static final String REPEAT_PASSWORD_ID = "new-password-repeat";
    public static final String CHANGE_LOGIN_PASSWORD = "change-login-password";
    public static final String CHANGE_PASSWORD_OLD_PASSWORD  = "old-password";
    public static final String SETTINGS_MESSAGE = "settingsMessages";

    public static final String ROLES_MESSAGE_ID = "rolesMessages";
    public static final String EDIT_USER_MESSAGE_ID = "editUserCardId";
    public static final String EDIT_DOOR_MESSAGE_ID = "editDoorLockId";

    public static final String USER_ADDED_LABEL = resourceBundle.getString("message.added_user.label");
    public static final String USER_ADDED_MESSAGE = resourceBundle.getString("message.added_user.message");
    public static final String DOOR_ADDED_LABEL = resourceBundle.getString("message.added_door.label");
    public static final String DOOR_ADDED_MESSAGE = resourceBundle.getString("message.added_door.message");
    public static final String GROUP_ADDED_LABEL = resourceBundle.getString("message.added_group.label");
    public static final String GROUP_ADDED_MESSAGE = resourceBundle.getString("message.added_group.message");
    public static final String ID_ALREADY_EXISTS_LABEL = resourceBundle.getString("message.id_already_exists.label");

    public static final String WRONG_REPEATED_PASSWORD = resourceBundle.getString("message.settings.wrong_repeated_pass");
    public static final String WRONG_PASSWORD = resourceBundle.getString("message.settings.wrong_pass");
    public static final String LOGIN_CHANGED_TITLE = resourceBundle.getString("message.settings.login_changed.title");
    public static final String LOGIN_CHANGED_DESCRIPTION = resourceBundle.getString("message.settings.login_changed.description");
    public static final String PASSWORD_CHANGED_TITLE = resourceBundle.getString("message.settings.password_changed.title");
    public static final String PASSWORD_CHANGED_DESCRIPTION = resourceBundle.getString("message.settings.password_changed.description");

    public static final String ROLE_ADDED_LABEL = resourceBundle.getString("message.added_role.label");
    public static final String ROLE_ADDED_MESSAGE = resourceBundle.getString("message.added_role.message");

}
