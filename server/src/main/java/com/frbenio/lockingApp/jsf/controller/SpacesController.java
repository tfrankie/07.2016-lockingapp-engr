package com.frbenio.lockingApp.jsf.controller;

import com.frbenio.lockingApp.database.model.Group;
import com.frbenio.lockingApp.database.model.Room;
import com.frbenio.lockingApp.database.model.Space;
import com.frbenio.lockingApp.database.service.SpaceService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by frben on 09.07.2016.
 */
@Component
@Scope("singleton")
public class SpacesController {

    private final SpaceService spaceService;

    @Autowired
    public SpacesController(SpaceService spaceService) {
        this.spaceService = spaceService;
    }

    public List<Group> getFirstTierGroups() {
        return spaceService.getFirstTierGroups();
    }

    public List<Room> getFirstTierRooms() {
        return spaceService.getFirstTierRooms();
    }

    public void save(Space newSpace) {
        spaceService.save(newSpace);
    }

    public void deleteRoom(Room room) {
        spaceService.deleteSpace(room);
    }

    public void deleteGroup(Group group) {
        if(group.getGroups() != null){
            for (Group singleGroup : group.getGroups()) {
                deleteGroup(singleGroup);
                spaceService.deleteSpace(singleGroup);
            }
        }
        if(group.getRooms() != null){
            group.getRooms().forEach(spaceService::deleteSpace);
        }
        spaceService.deleteSpace(group);
    }

    public Group getGroupById(ObjectId mongoId) {
        return spaceService.getGroupById(mongoId);
    }

    public boolean idAlreadyExists(Room editingRoom) {
        Room roomById = spaceService.findRoomByLockId(editingRoom.getLockId());
        if(roomById == null){
            return false;
        }
        return !roomById.getMongoId().equals(editingRoom.getMongoId());
    }
}
