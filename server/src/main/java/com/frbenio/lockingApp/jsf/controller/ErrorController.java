package com.frbenio.lockingApp.jsf.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.context.FacesContext;

/**
 * Created by frben on 04.09.2016.
 */
@Component
@Scope("request")
public class ErrorController {

    public String getStatusCode(){
        return String.valueOf((Integer) FacesContext.getCurrentInstance().getExternalContext().
                getRequestMap().get("javax.servlet.error.status_code"));
    }

}
