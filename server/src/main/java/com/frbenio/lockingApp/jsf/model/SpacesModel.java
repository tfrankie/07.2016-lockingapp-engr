package com.frbenio.lockingApp.jsf.model;

import com.frbenio.lockingApp.database.model.Group;
import com.frbenio.lockingApp.database.model.Room;
import com.frbenio.lockingApp.jsf.controller.SpacesController;
import com.frbenio.lockingApp.jsf.utils.BreadCrumbItem;
import com.frbenio.lockingApp.jsf.utils.consts.Messages;
import com.frbenio.lockingApp.jsf.utils.consts.ParamId;
import com.frbenio.lockingApp.jsf.utils.consts.ViewId;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by frben on 22.10.2016.
 */
@Component
@Scope("view")
public class SpacesModel implements Serializable {

    private final SpacesController controller;

    private Room newRoom = new Room();
    private Room copyOfEditingRoom;
    private Room editingRoom;

    private Group newGroup = new Group();
    private Group copyOfEditingGroup;
    private Group editingGroup;

    private Group currentTierGroup;
    private List<Group> currentTierGroups;
    private List<Room> currentTierRooms;

    private List<BreadCrumbItem> breadCrumbModel;
    private int addDecision = 1;

    @Autowired
    public SpacesModel(SpacesController controller) {
        this.controller = controller;
    }

    @PostConstruct
    private void init() {
        currentTierGroup = (Group) FacesContext.getCurrentInstance().getExternalContext().getFlash().get(ParamId.GROUP);

        if (currentTierGroup == null) {
            currentTierGroups = controller.getFirstTierGroups();
            currentTierRooms = controller.getFirstTierRooms();
        } else {
            currentTierGroups = currentTierGroup.getGroups();
            currentTierRooms = currentTierGroup.getRooms();

            breadCrumbModel = (List<BreadCrumbItem>) FacesContext.getCurrentInstance().getExternalContext().getFlash().get(ParamId.BREADCRUMB_MODEL);
        }
    }

    public void onRowSelect(SelectEvent event) throws IOException {
        Group group = (Group) event.getObject();

        if(breadCrumbModel == null){
            breadCrumbModel = new ArrayList<>();
        }
        breadCrumbModel.add(new BreadCrumbItem(group.getMongoId(), group.getName()));

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Flash flash = facesContext.getExternalContext().getFlash();
        flash.put(ParamId.GROUP, group);
        flash.put(ParamId.BREADCRUMB_MODEL, breadCrumbModel);
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, ViewId.SPACES);
    }

    public void addRoom() {
        if (controller.idAlreadyExists(newRoom)) {
            FacesContext.getCurrentInstance().addMessage(Messages.NEW_DOOR_MESSAGE_ID, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.ID_ALREADY_EXISTS_LABEL, null));
        } else {
            RequestContext.getCurrentInstance().execute("PF('addDialog').hide();");

            if(currentTierGroup == null){
                if(currentTierRooms == null){
                    currentTierRooms = new ArrayList<>();
                }
                currentTierRooms.add(newRoom);

                newRoom.setRoot(true);
                controller.save(newRoom);
            } else {
                if(currentTierGroup.getRooms() == null){
                    currentTierGroup.setRooms(new ArrayList<>());
                    currentTierRooms = currentTierGroup.getRooms();
                }
                currentTierGroup.getRooms().add(newRoom);
                controller.save(newRoom);
                controller.save(currentTierGroup);
            }
            FacesContext.getCurrentInstance().addMessage(Messages.DOOR_MESSAGE_ID, new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.DOOR_ADDED_LABEL, Messages.DOOR_ADDED_MESSAGE));
        }

        newRoom = new Room();
    }

    public void addGroup(){
        RequestContext.getCurrentInstance().execute("PF('addDialog').hide();");

        if(currentTierGroup == null){
            if(currentTierGroups == null){
                currentTierGroups = new ArrayList<>();
            }
            currentTierGroups.add(newGroup);

            newGroup.setRoot(true);
            controller.save(newGroup);
        } else {
            if(currentTierGroup.getGroups() == null){
                currentTierGroup.setGroups(new ArrayList<>());
                currentTierGroups = currentTierGroup.getGroups();
            }
            currentTierGroup.getGroups().add(newGroup);
            controller.save(newGroup);
            controller.save(currentTierGroup);

        }
        FacesContext.getCurrentInstance().addMessage(Messages.DOOR_MESSAGE_ID, new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.GROUP_ADDED_LABEL, Messages.GROUP_ADDED_MESSAGE));

        newGroup = new Group();
    }

    public void editRoom(Room room) {
        editingRoom = room;
        copyOfEditingRoom = new Room(editingRoom);
        RequestContext.getCurrentInstance().execute("PF('editRoomDialog').show();");
    }

    public void editGroup(Group group) {
        editingGroup = group;
        copyOfEditingGroup = new Group(editingGroup);
        RequestContext.getCurrentInstance().execute("PF('editGroupDialog').show();");
    }

    public void deleteGroup(Group group) {
        if(currentTierGroup != null){
            currentTierGroup.getGroups().remove(group);
            controller.save(currentTierGroup);
        }
        currentTierGroups.remove(group);
        controller.deleteGroup(group);
    }

    public void deleteRoom(Room room) {
        if(currentTierGroup != null){
            currentTierGroup.getRooms().remove(room);
            controller.save(currentTierGroup);
        }
        currentTierRooms.remove(room);
        controller.deleteRoom(room);
    }

    public String openGroup(BreadCrumbItem groupItem) {
        int groupIndex = breadCrumbModel.indexOf(groupItem);
        for(int i=breadCrumbModel.size()-1; i>=0; i--){
            if(i > groupIndex){
                breadCrumbModel.remove(i);
            }
        }

        Group group = controller.getGroupById(groupItem.getMongoId());

        Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
        flash.put(ParamId.GROUP, group);
        flash.put(ParamId.BREADCRUMB_MODEL, breadCrumbModel);
        return ViewId.SPACES;
    }


    public void saveRoom(){
        if(controller.idAlreadyExists(copyOfEditingRoom)){
            FacesContext.getCurrentInstance().addMessage(Messages.EDIT_DOOR_MESSAGE_ID, new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.ID_ALREADY_EXISTS_LABEL, null));
        } else {
            RequestContext.getCurrentInstance().execute("PF('editRoomDialog').hide();");
            controller.save(copyOfEditingRoom);
            applyEditedRoom();
        }
    }

    private void applyEditedRoom() {
        editingRoom.setName(copyOfEditingRoom.getName());
        editingRoom.setLockId(copyOfEditingRoom.getLockId());
    }

    public void saveGroup(){
        RequestContext.getCurrentInstance().execute("PF('editGroupDialog').hide();");
        controller.save(copyOfEditingGroup);
        editingGroup.setName(copyOfEditingGroup.getName());
    }


    public boolean disableFold(BreadCrumbItem groupItem){
        return groupItem.getMongoId().equals(currentTierGroup.getMongoId());
    }

    public List<Group> getCurrentTierGroups() {
        return currentTierGroups;
    }

    public void setCurrentTierGroups(List<Group> currentTierGroups) {
        this.currentTierGroups = currentTierGroups;
    }

    public List<Room> getCurrentTierRooms() {
        return currentTierRooms;
    }

    public void setCurrentTierRooms(List<Room> currentTierRooms) {
        this.currentTierRooms = currentTierRooms;
    }

    public List<BreadCrumbItem> getBreadCrumbModel() {
        return breadCrumbModel;
    }

    public int getAddDecision() {
        return addDecision;
    }

    public void setAddDecision(int addDecision) {
        this.addDecision = addDecision;
    }

    public Group getCurrentTierGroup() {
        return currentTierGroup;
    }

    public void setCurrentTierGroup(Group currentTierGroup) {
        this.currentTierGroup = currentTierGroup;
    }

    public Room getNewRoom() {
        return newRoom;
    }
    public void setNewRoom(Room newRoom) {
        this.newRoom = newRoom;
    }
    public Group getNewGroup() {
        return newGroup;
    }
    public void setNewGroup(Group newGroup) {
        this.newGroup = newGroup;
    }
    public Room getCopyOfEditingRoom() {
        return copyOfEditingRoom;
    }
    public void setCopyOfEditingRoom(Room copyOfEditingRoom) {
        this.copyOfEditingRoom = copyOfEditingRoom;
    }
    public Group getCopyOfEditingGroup() {
        return copyOfEditingGroup;
    }
    public void setCopyOfEditingGroup(Group copyOfEditingGroup) {
        this.copyOfEditingGroup = copyOfEditingGroup;
    }
}
