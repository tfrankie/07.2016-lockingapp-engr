package com.frbenio.lockingApp.jsf.controller;

import com.frbenio.lockingApp.database.service.AdminService;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by frben on 25.09.2016.
 */
@Component
@Scope("singleton")
public class SettingController {

    private AdminService adminService;

    @Autowired
    public SettingController(AdminService adminService) {
        this.adminService = adminService;
    }

    public void changeLogin(String newLogin){
        adminService.updateLogin(newLogin);
    }

    public void changePassword(String newPassword){
        adminService.updatePassword(newPassword);
    }

    public boolean validateCurrentPassword(String password){
        return adminService.validateCurrentPassword(password);
    }

    public StreamedContent getLogsFile() throws FileNotFoundException {
        File logsFile = new File("C:\\Users\\frben\\workspace\\inż\\lockingApp\\requests.log");
        InputStream stream = new FileInputStream(logsFile);
        return new DefaultStreamedContent(stream, "text/plain", "requests.log");
    }

}
