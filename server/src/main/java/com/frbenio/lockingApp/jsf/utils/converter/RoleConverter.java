package com.frbenio.lockingApp.jsf.utils.converter;

import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.database.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.List;

/**
 * Created by frben on 24.10.2016.
 */
@Component
@Scope("view")
public class RoleConverter implements Converter, Serializable {

    private List<Role> allRoles;

    @Autowired
    public RoleConverter(RolesService rolesService) {
        allRoles = rolesService.getRoles();
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        for(Role role: allRoles){
            if(String.valueOf(role.getMongoId()).equals(s)){
                return role;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if(o == null){
            return null;
        }
        return String.valueOf(((Role) o).getMongoId());
    }

}
