package com.frbenio.lockingApp.jsf.controller;

import com.frbenio.lockingApp.database.model.Role;
import com.frbenio.lockingApp.database.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by frben on 24.10.2016.
 */
@Component
@Scope("singleton")
public class RolesController {

    private final RolesService rolesService;

    @Autowired
    public RolesController(RolesService rolesService) {
        this.rolesService = rolesService;
    }

    public List<Role> getRoles() {
        return rolesService.getRoles();
    }

    public void addRole(Role newRole) {
        rolesService.addRole(newRole);
    }

    public void deleteRole(Role role) {
        rolesService.deleteRole(role);
    }
}
